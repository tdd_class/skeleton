from flask_ini import FlaskIni
from my_tdd_app.app import app, model


if __name__ == '__main__':
    app.iniconfig = FlaskIni()
    with app.app_context():
        app.iniconfig.read('instance/config.ini')
        model.db.init_app(app)
        model.db.create_all()
        u = model.User(username='testuser', email='test@test.com')
        model.db.session.add(u)
        model.db.session.commit()
    app.run()
