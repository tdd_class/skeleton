import math


def add(a, b):
    # Add two numbers and return the result
    return a+b


def sqrt(num):
    # Return the square root of a number
    return math.sqrt(num)
