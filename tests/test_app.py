from unittest import TestCase
from my_tdd_app.app import app, model
import json


class MyFirstTest(TestCase):
    """
    Simple Test Class to test the Flask application.
    """

    def setUp(self):
        """
        setUp is called before every test function.
        It is used to do some basic setup stuff before you run the
        actual test.
        """
        self.app = app

        # This is some magic to get a Flask context
        ctx = self.app.app_context()
        ctx.push()

        """
        We also need to setup the database and create all tables.
        In this case because we don't configure anything SQLAlchemy will use
        an SQLite in-memory database.
        """
        model.db.init_app(app)
        model.db.create_all()
        model.db.session.commit()

        # Setup the client that we will use in the tests
        self.client = self.app.test_client()

    def test_hello_world(self):
        """
        Test the hello world function.
        """
        # Add a test user first.
        testuser = model.User(username='some_test_user',
                              email='test@nightowl.foundationu.com')
        model.db.session.add(testuser)
        model.db.session.commit()

        # Now send an HTTP request to the application
        resp = self.client.get('/some_test_user')

        # Load the JSON response into a python dictionary
        resp_data = json.loads(resp.get_data(as_text=True))

        # Now test the resulting data
        self.assertEqual(resp_data['email'], testuser.email)
        self.assertEqual(resp_data['id'], testuser.id)
        self.assertEqual(resp_data['message'], 'Hello %s!' % testuser.username)
